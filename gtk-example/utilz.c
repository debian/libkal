/* The 'libkal' library for date conversion.
 * Copyright (C) 1996-1998 Petr Tomasek <tomasek@etf.cuni.cz>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  An X-windows example for the 'libkal' library. 
 *  (Uses the gtk library.)
 *
 */

#include <stdio.h>
#include "kal.h"
#include <gtk/gtk.h>

const char * days[]= { "Sun","Mon","Tue","Wed","Thu","Fri","Sat" };
const char * mons[]= { "January","February","March","April","May",
 "June","July","August","September","October","November","December" };

char * month_name(int m, int sys) {
if (kal_proc_valid(KAL_PROCS_MONTHS,sys))
 return mons[m-1]; /* 'sys' does not have the 'kal_xxx_months' function */
else
 return kal_xxx_months(m,sys);
}

GtkWidget * dni () {
 GtkWidget * table; 
 GtkWidget * label;
 int i;

 table=gtk_table_new(7,1,TRUE);

for (i=1;i<=7;i++) {
 label=gtk_label_new(days[i-1]);
 gtk_table_attach_defaults(GTK_TABLE(table),label,0,1,i-1,i);
 gtk_widget_show(label);
   }
return table;
}

void mesyc (char mes[50], long jd, int sys) {
int d,m,y;
int before;
char * bf="";
kal_conv_jd_xxx(jd,&d,&m,&y,sys);
y=kal_year_decode(y,&before);
if (before) bf=" before...";
printf ("<%s>>%s %d%s\n", kal_proc_get_name(sys), month_name(m,sys),y,bf);
fflush(stdout);
sprintf (mes,"%s %d%s", month_name(m,sys),y,bf);
}


GtkWidget * tblka (GtkWidget *tbla, GtkWidget *label[7][6],long jd,int sys) {
 long jd1,jd2,jdd;
 int i,j;
 char num[10];

 GtkWidget * table; 

 
 int day,month,year,m,y;
 int cols;
 
 
 kal_conv_jd_xxx(jd,&day,&month,&year,sys);

 jd2=kal_conv_xxx_jd(1,month+1,year,sys);
 jdd=kal_conv_xxx_jd(1,month,year,sys);

 jd1=kal_prev_dw(kal_conv_xxx_jd(1,month,year,sys),0); 
    /* nearest (previous) Sunday to the beginning of the month */
       
       

if (tbla==NULL) 
 {
 table=gtk_table_new(7,6,TRUE);
 for(i=1; i<=7; i++) for (j=1; j<=6; j++) 
  {
 label[i-1][j-1]=gtk_label_new(" ");
 gtk_table_attach_defaults(GTK_TABLE(table),label[i-1][j-1],j-1,j,i-1,i);
 gtk_widget_show(label[i-1][j-1]);
  }
 }
 else 
 {
 table=tbla;
 for(i=1; i<=7; i++) for (j=1; j<=6; j++) 
  {
 gtk_widget_hide (label[i-1][j-1]);
 gtk_label_set (GTK_LABEL(label[i-1][j-1])," ");
 gtk_widget_show(label[i-1][j-1]);
  }
 }
 

for (i=1; i<=7; i++) {
 j=0;
 for(jdd=jd1+i-1;jdd<jd2;jdd=jdd+7) { 
  kal_conv_jd_xxx(jdd,&day,&m,&y,sys);
  if (m==month) sprintf(num,"  %d ",day);
   else sprintf(num,"    ");
 gtk_label_set(label[i-1][j],(num));
 gtk_widget_hide(label[i-1][j]);
 gtk_widget_show(label[i-1][j]);
 j++; 
 }
  }

return table;
 }

