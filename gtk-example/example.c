/* The 'libkal' library for date conversion.
 * Copyright (C) 1996-1998 Petr Tomasek <tomasek@etf.cuni.cz>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  An X-windows example for the 'libkal' library. 
 *  (Uses the gtk library.)
 *
 */

#include <gtk/gtk.h>
#include <gdk/gdk.h>

#include "utilz.c"

long jda;   /* The jd of (some) day of the displayed month */
int sys;    /* Calendary system */

GtkWidget *kalendar;
GtkWidget *labl [7][6];
GtkWidget *frame;
static GtkWidget * radio[KAL_XXX];

char mes[50];


void destr (void) {
gtk_exit(0);
}

void k_upd () {
GtkWidget * kal;
mesyc(mes,jda,sys);
gtk_frame_set_label(GTK_FRAME(frame),mes);
kalendar=tblka(kalendar,labl,jda,sys);
} 


void nxt (void) {  /* next month */
int d,m,y;
int ms=12;
if (sys==KAL_JEW) ms=13;   /* jewish calendar has (sometimes) 13 months */
kal_conv_jd_xxx(jda,&d,&m,&y,sys);

/* JEW: skip the leap month if not leap year */
if ((m==6) && (sys==KAL_JEW) && (!kal_jew_leap(y))) m++;

if (m==ms) { y++; m=1; } else m++; 

jda=kal_conv_xxx_jd(d,m,y,sys);
k_upd ();
}



void prv (void) {  /* previous month */
int d,m,y;
int ms=12;
if (sys==KAL_JEW) ms=13;   /* jewish calendar has (sometimes) 13 months */
kal_conv_jd_xxx(jda,&d,&m,&y,sys);

/* JEW: skip the leap month if not leap year */
if ((m==8) && (sys==KAL_JEW) && (!kal_jew_leap(y))) m--;

if (m==1) { y--; m=ms; } else m--;
  
jda=kal_conv_xxx_jd(d,m,y,sys);
k_upd ();
}



void nxty (void) { /* next year */
int d,m,r;
kal_conv_jd_xxx(jda,&d,&m,&r,sys);
r++; 
jda=kal_conv_xxx_jd(d,m,r,sys);
k_upd ();
}



void prvy (void) { /* previous year */
int d,m,r;
kal_conv_jd_xxx(jda,&d,&m,&r,sys);
r--;
jda=kal_conv_xxx_jd(d,m,r,sys);
k_upd ();
}


void change_sys (void) {  /* switch to another calendary system */ 
int i,j=0;
for(i=0; i<KAL_XXX;i++)
  if (radio[i])
    if (GTK_TOGGLE_BUTTON(radio[i])->active) j=i;
if (j!=sys)
 {
 sys=j;
 k_upd ();
 }
}



GtkWidget * radios() {  /* draw radio-buttons */
GtkWidget * table;
GtkWidget * label;
GSList * group;
int i;

// ---------
label=gtk_label_new("Calendar :");
table=gtk_table_new(KAL_XXX+1,1,TRUE);
gtk_table_attach_defaults(GTK_TABLE(table),label,0,1,0,1);
gtk_widget_show(label);

 for(i=0; i<KAL_XXX; i++)
  {
 if (!i)
   radio[i]=gtk_radio_button_new_with_label(NULL,kal_proc_get_name(0));
 else 
   {
   group=gtk_radio_button_group(GTK_RADIO_BUTTON(radio[i-1]));     
   radio[i]=gtk_radio_button_new_with_label(group,kal_proc_get_name(i));       
   }
 gtk_signal_connect (GTK_OBJECT(radio[i]), "clicked",
	  GTK_SIGNAL_FUNC(change_sys), NULL );
 gtk_table_attach_defaults(GTK_TABLE(table),radio[i],0,1,i+1,i+2);
 if (sys==i) gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(radio[i]), TRUE);
 gtk_widget_show(radio[i]);
  }
return table;
}


int main (int argc, char * argv[]) {
GtkWidget *window;
GtkWidget *label;
GtkWidget *button;
GtkWidget *table;
GtkWidget *box,*sbox;
GtkWidget *separ;
//GtkWidget *kalendar;
gdouble value;
char a;

GdkEvent * event; 

GdkColormap * cmap;
GdkColor *black,*white;
GdkFont *font;

/* ----------		*/
sys=KAL_GRE; /* We begin with the gregorian calendar */

gtk_init (&argc,&argv); 

/* -----window 		*/
window= gtk_window_new(GTK_WINDOW_TOPLEVEL);
gtk_container_border_width(GTK_CONTAINER (window), 12);

/* ----box 		*/
box= gtk_hbox_new(FALSE,20);
gtk_container_add (GTK_CONTAINER(window), box);

/* ----- radio-buttons 	*/
table=radios();
gtk_box_pack_start_defaults(GTK_BOX(box),table);
gtk_widget_show (table);


/* ----separator	*/
separ=gtk_vseparator_new();
gtk_box_pack_start_defaults(GTK_BOX(box),separ);
gtk_widget_show (separ);

//----kalendar
jda=kal_getdate();


 //--frame:
 mesyc(mes,jda,sys);
 frame=gtk_frame_new(mes);
 gtk_box_pack_start_defaults(GTK_BOX(box),frame);
 gtk_widget_show(frame);
 sbox=gtk_hbox_new(FALSE,10);
 gtk_container_add(GTK_FRAME(frame),sbox);
 gtk_container_border_width(GTK_FRAME(frame),8);
 gtk_widget_show(sbox);

kalendar=dni();
gtk_box_pack_start_defaults(GTK_BOX(sbox),kalendar);
gtk_widget_show(kalendar);
labl[1][5]=separ;
kalendar=tblka(NULL,labl,jda,sys);
gtk_box_pack_start_defaults(GTK_BOX(sbox),kalendar);
gtk_widget_show(kalendar);

//----separator
separ=gtk_vseparator_new();
gtk_box_pack_start_defaults(GTK_BOX(box),separ);
gtk_widget_show (separ);

//---- < >
table=gtk_table_new(4,1,TRUE);


 //----button
 button=gtk_button_new_with_label(" > ");
 gtk_table_attach_defaults(GTK_TABLE(table),button,0,1,0,1);
 gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(nxt),NULL); 
 gtk_widget_show (button);

 //----button
 button=gtk_button_new_with_label(" < ");
 gtk_table_attach_defaults(GTK_TABLE(table),button,0,1,1,2);
 gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(prv),NULL); 
 gtk_widget_show (button);

 //----button
 button=gtk_button_new_with_label(" >> ");
 gtk_table_attach_defaults(GTK_TABLE(table),button,0,1,2,3);
 gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(nxty),NULL); 
 gtk_widget_show (button);

 //----button
 button=gtk_button_new_with_label(" << ");
 gtk_table_attach_defaults(GTK_TABLE(table),button,0,1,3,4);
 gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(prvy),NULL); 
 gtk_widget_show (button);


gtk_box_pack_start_defaults(GTK_BOX(box),table);
gtk_widget_show (table);

//----button
button=gtk_button_new_with_label("O.K.");
gtk_box_pack_start_defaults(GTK_BOX(box),button);
gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC (destr),NULL); 
gtk_widget_show (button);

gtk_widget_show (box);

gtk_signal_connect(GTK_OBJECT(window), "destroy", GTK_SIGNAL_FUNC (destr), NULL);

gtk_widget_show(window);

gtk_main();
return 0;
}

