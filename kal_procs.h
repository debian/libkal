/* The 'libkal' library for date conversion.
 * Copyright (C) 1996-1998 Petr Tomasek <tomasek@etf.cuni.cz>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _KAL_PROCS_H__
#define _KAL_PROCS_H__


#include "kal_main.h"
#include "kal_names.h"

#ifndef NULL
#define NULL ((void *) 0)
#endif

/*
 * Numbers of calendary systems.
 */

#define KAL_JUL 0
#define KAL_GRE 1
#define KAL_AR 2
/* #define KAL_TUR 3 */
#define KAL_JEW 3

#define KAL_XXX 4 /* How many calednars do we know ? */

/*
 *  Procedure table. When adding a new calendary system, add a new
 *   entry here.
 */

#define KAL_PROCS_NAME 0
#define KAL_PROCS_XXX_JD 1
#define KAL_PROCS_JD_XXX 2
#define KAL_PROCS_YEAR_INFO 3
#define KAL_PROCS_MONTHS 4
#define KAL_PROCS_EAST 5


static const void * kal_procs[][6] =
 { {	(void *) "jul",      /* Julianic */
	kal_conv_jul_jd,  	/* kal_conv_xxx_jd */
	kal_conv_jd_jul,	/* kal_conv_jd_xxx */
	NULL,			/* kal_xxx_year_info (NOT IMPLEMENTED YET) */
	NULL,              	/* kal_xxx_months */
	&kal_jul_east },	/* kal_xxx_east */

   {	(void *) "gre",	     /* Gregorianic */
	kal_conv_gre_jd,
	kal_conv_jd_gre,
	NULL,
	NULL,
	kal_jul_east },

  {	(void *) "ar",       /* Arabic */
	kal_conv_ar_jd,
	kal_conv_jd_ar,
	NULL,
	kal_ar_months,
	NULL         },

/*  {	(void *) "tur",       * Turkish * (NOT IMPLEMENTED)
	kal_conv_tur_jd,
	kal_conv_jd_tur,
	NULL,
	kal_ar_months,
	NULL         },      */

  {	(void *) "jew",	     /* Jewish */
	kal_conv_jew_jd,
	kal_conv_jd_jew,
	NULL,
	kal_jew_months,
	NULL,	     } };


/*  
 *  Procedures in general form.
 *
 */

int kal_proc_valid(int proc, int sys);
char * kal_proc_get_name(int sys);

long kal_conv_xxx_jd(int d, int m, int y, int sys);
void kal_conv_jd_xxx(long jd, int *d, int *m, int *y, int sys);
long kal_xxx_east(int y, int sys);
char * kal_xxx_months(int m, int sys);

#endif
