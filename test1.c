/* The 'libkal' library for date conversion.
 * Copyright (C) 1996-1998 Petr Tomasek <tomasek@etf.cuni.cz>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * A (very) simple example II.
 */

#include "kal.h"

const char * dw[]=
{"Sunday","Monday","Tuestday","Wendsday","Thurstday","Friday","Saturday"};

void get_date(int *d, int *m, int *y) 
{
printf("Day:");
scanf("%d",d);
printf("Month:");
scanf("%d",m);
printf("Year:");
scanf("%d",y);
}

int whatdate()
{
char in[100];
while (1) 
 {
printf("\n\nSelect what kind of date You want to enter "
       "[jul, gre, ar, jew, today]:\n");
scanf("%s",in);
if (!strcmp(in,"jul")) return 0;
if (!strcmp(in,"gre")) return 1;
if (!strcmp(in,"ar")) return 2;
if (!strcmp(in,"jew")) return 3;
if (!strcmp(in,"today")) return 4;
if (!strcmp(in,"quit")) exit(0);
if (!strcmp(in,"exit")) exit(0);
if (!strcmp(in,"x")) exit(0);
if (!strcmp(in,"q")) exit(0);
printf("Invalid choice !\n");
 }
}

void main() {
int d,m,y;
long jd,jd1;
int choice;

/*
 * Input:
 */

if ((choice=whatdate())<4)
 {
printf("\nInput a date in the ");
switch (choice) {
 case 0:  printf("julianic system:\n"); break;
 case 1:  printf("gregorianic system:\n"); break;
 case 2:  printf("arabic system:\n"); break;
 case 3:  printf("jewish system:\n"); 
	  printf(" (NOTE that the 8th month is always 'Nisan' here,\n"
		 "  even if the year has only 12 months!)\n");
          break;
                }
get_date(&d,&m,&y);
 }


if (choice>3)
 {
jd=kal_getdate(); /* get today's date */
 }
else

/* Now , how easy: The "kal_xxx_check" function :
	1) checks if the date is valid.
	2) converts it from the 'choice' calendar format [see kal_procs.h]
		to jd. */

if (kal_xxx_check(&jd, d, m, y,choice))
   {
  printf("\n Invalid date !\n");
  exit(-1);
   }


/* 
 * Output :
 */

printf("\n===============================================================================\n");
printf("Julianic date jd=%ld\t\tDay of week: %s\n",jd,dw[kal_day_of_week(jd)]);
printf("===============================================================================\n\n");

kal_conv_jd_jul(jd,&d,&m,&y);
printf("Date in Julianic system: %d.%d.%d\n",d,m,y);
jd1=kal_jul_east(y);
kal_conv_jd_jul(jd1,&d,&m,&y);
printf("(Easter sunday this year: %d.%d.%d)\n\n",d,m,y);


kal_conv_jd_gre(jd,&d,&m,&y);
printf("Date in Gregorianic system: %d.%d.%d\n",d,m,y);
jd1=kal_gre_east(y);
kal_conv_jd_gre(jd1,&d,&m,&y);
printf("(Easter sunday this year: %d.%d.%d)\n\n",d,m,y);


kal_conv_jd_ar(jd,&d,&m,&y);
printf("Date in Arabic system: %d. %s %d\n\n",d,kal_ar_months(m),y);

kal_conv_jd_jew(jd,&d,&m,&y);
printf("Date in Jewish system: %d. %s %d\n\n",d,kal_jew_months(m),y);
}
