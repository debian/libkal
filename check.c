/* The 'libkal' library for date conversion.
 * Copyright (C) 1996-1998 Petr Tomasek <tomasek@etf.cuni.cz>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  Basic checks for the libkal library.
 *
 */

#include "kal.h"
#include <stdio.h>

long jd,jd1;
int d,m,y;
int dd,mm,yy;
long errs;


void help(void) {
int i;

printf(
"Tester of the libkal library. (c) 1998 Petr Tomasek.\n"
"Usage: check <calendar>\n\n"
",where <calendar> can be one of:");
for (i=0;i<KAL_XXX;i++) 
 {
 if (i) putchar(',');
 printf(" '%s'",kal_proc_get_name(i));
 }
printf(
"\nIt displays progress information on the stdout and possibly errors\n"
"(in the library) on the stderr.\n\n"
);

exit(-1);
}

void test2er (void) {
 fprintf(stderr,"TEST2: %ld (%d/%d/%d after %d/%d/%d) Error ! \n",
	 jd,d,m,y,dd,mm,yy);
 errs++;
}


void main (int argc, char **argv) {
int sys=-1,i;

if (argc!=2) help();
for (i=0;i<KAL_XXX;i++)
 {
 if (!strcmp(argv[1],kal_proc_get_name(i))) sys=i;
 }
if (sys==-1) help();
printf("Checking %s.\n\nJulianic date:",argv[1]);

kal_conv_jd_xxx(-1000001,&dd,&mm,&yy,sys); /* prepare dd/mm/yy */

/* Tests between Sun 28.Dec 7452 B.C and Fri 12.Jul 6239 A.D. 
   Should be enought for our purposes ;-) */
for (jd=-1000000; jd<=4000000; jd++)  /* basic testing cycle - */
{
/* First, display progress info */
if (!(jd%40000)) printf("\n%ld    \t",jd);
if (!(jd%1000)) putchar ('.');
fflush(stdout);

/* Second, try to convert jd to d/m/y and then back into jd1.
   jd and jd1 should equal. */
kal_conv_jd_xxx(jd,&d,&m,&y,sys);
jd1=kal_conv_xxx_jd(d,m,y,sys);

if (jd!=jd1) {
	fprintf(stderr,"TEST1: %ld (!= %ld) Error !!\n", jd,jd1);
	errs++;
	     }

/* Finaly check it for the consistency */
if ((d!=dd+1) || (m!=mm) || (y!=yy))     
/* today's day number should be normally the yesterday's one +1 */
  {
   if (d!=1)      /* when not, today is probably the 1th in the month */
      test2er();   /* NO, It isn't -> error ! */
   else
   {
    if ((m!=mm+1) || (y!=yy))  /* simmillar for months */
    {
     if (m!=1)   
     {
      /* workaround for the jewish calendar */
      if ((sys!=KAL_JEW) || (m!=8) || (mm!=6) || (kal_jew_leap(y)))
       test2er();   /* error ! */ 
     }
     else if (y!=yy+1) test2er();
    }    
   } 
  }
dd=d;mm=m;yy=y;      
}

printf("\n\n %ld errors found.\n",errs);

}
