/* The 'libkal' library for date conversion.
 * Copyright (C) 1996-1998 Petr Tomasek <tomasek@etf.cuni.cz>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  This provides checks, whether given date is valid.
 *  Note, that this already converts the date to the jd, so that
 *  you need not to convert it again.
 */

#include "kal_main.h"
#include "kal_procs.h"

int kal_general_check(int d,int m,int y) {
if ((d<1) || (d>32)) return 1; /* does anybody know about calendar
				  system, that can have more than
				  31 days per month ? */
if ((m<0) || (m>13)) return 1; /* does anybody know about calendar
				  system, that can have more then
				  13 months per year ? */
return 0; 
}


/*
 *  kal_xxx_check
 */

int kal_xxx_check(long *jd, int d, int m, int y, int sys) {
int dd,mm,yy;

if (kal_general_check(d,m,y)) return 1;

*jd=kal_conv_xxx_jd(d,m,y,sys);
kal_conv_jd_xxx(*jd,&dd,&mm,&yy,sys);
if ((d!=dd) || (m!=mm) || (y!=yy)) return 1;

return 0;
}


/* julianic */
int kal_jul_check(long *jd, int d, int m, int y) {
return kal_xxx_check (jd, d, m, y, KAL_JUL);
}

/* gregorianic */
int kal_gre_check(long *jd, int d, int m, int y) {
return kal_xxx_check (jd, d, m, y, KAL_GRE);
}

/* arabic */
int kal_ar_check(long *jd, int d, int m, int y) {
return kal_xxx_check (jd, d, m, y, KAL_AR);
}

/* jewish */
int kal_jew_check(long *jd, int d, int m, int y) {
return kal_xxx_check (jd, d, m, y, KAL_JEW);
}

