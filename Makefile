

CC=gcc
AR=ar
RANLIB=ranlib
#OPT=-O2
OPT=-g -Wall
AROPT=rc
LIBS=-L./ -lkal

#########################################################################


all:tests libkal.a
tests: check test test1 test-dw

test-dw: test-dw.c libkal.a
	$(CC) $(OPT) test-dw.c libkal.a -o $@ $(LIBS)

test1: test1.c libkal.a
	$(CC) $(OPT) test1.c libkal.a -o $@ $(LIBS)

test: test.c libkal.a
	$(CC) $(OPT) test.c libkal.a -o $@ $(LIBS)

check: check.c libkal.a
	$(CC) $(OPT) check.c libkal.a -o $@ $(LIBS)

libkal.a: kal_main.o kal_getdate.o kal_names.o kal_check.o kal_procs.o
	rm -f $@
	$(AR) $(AROPT) $@ kal_main.o kal_getdate.o kal_names.o kal_check.o                                kal_procs.o
	$(RANLIB) $@
	
clean: 
	rm -f *.o *~ core check test test1 libkal.a test-dw

