/* The 'libkal' library for date conversion.
 * Copyright (C) 1996-1998 Petr Tomasek <tomasek@etf.cuni.cz>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdio.h>

/* 
 * Get current date (jd)
 *
 * FIXME : Is it possible to do this without having to exec 'date' ?
 *
 */

long kal_getdate () {
  FILE * fil;
 char str[100];
  int day,month,year;
 
  fil=popen("date +%d%n%m%n%Y%n%H%n%M%n%S%n%w%n%j%n%Z%n","r");
  fscanf(fil,"%s",str);day=atoi(str);
  fscanf(fil,"%s",str);month=atoi(str);
  fscanf(fil,"%s",str);year=atoi(str);
  pclose(fil);

 return kal_conv_gre_jd(day,month,year);
 }

