/* The 'libkal' library for date conversion.
 * Copyright (C) 1996-1998 Petr Tomasek <tomasek@etf.cuni.cz>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 *   An example for day-of-week manipulation.
 */ 

#include "kal.h"


void main() {
const char * dw[]={"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
long jd,jd1;
int i,j;
int d,m,y;

jd=kal_getdate(); /* get current date */

for (j=0; j<=6; j++)
for (i=0; i<=19; i++) 
{
jd1=jd+i;
if (!i) printf 
("\n --->%s<--\n <Day> \t    |\t prev\t\t prev_before\t next\t\t next_after\n"
"============+===============================================================\n",dw[j]);
kal_conv_jd_gre(jd1,&d,&m,&y);
printf(" %s %d/%d   |",dw[kal_day_of_week(jd1)],d,m);

jd1=kal_prev_dw(jd1,j); 
kal_conv_jd_gre(jd1,&d,&m,&y);
printf("\t %s %d/%d",dw[kal_day_of_week(jd1)],d,m);

jd1=kal_prev_before_dw(jd+i,j);
kal_conv_jd_gre(jd1,&d,&m,&y);
printf("\t %s %d/%d",dw[kal_day_of_week(jd1)],d,m);

jd1=kal_next_dw(jd+i,j);
kal_conv_jd_gre(jd1,&d,&m,&y);
printf("\t %s %d/%d",dw[kal_day_of_week(jd1)],d,m);

jd1=kal_next_after_dw(jd+i,j);
kal_conv_jd_gre(jd1,&d,&m,&y);
printf("\t %s %d/%d\n",dw[kal_day_of_week(jd1)],d,m);
}


}
