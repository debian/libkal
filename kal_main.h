/* The 'libkal' library for date conversion.
 * Copyright (C) 1996-1998 Petr Tomasek <tomasek@etf.cuni.cz>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _KAL_MAIN_H__
#define _KAL_MAIN_H__

/* 
 * divide/modulo 
 */
long kal_dv(long a, long b);
long kal_md(long a, long b);


/* 
 * Julianic calendar 
 */
long kal_conv_jul_jd(int d,int m, int y);
void kal_conv_jd_jul(long jd,int *d,int *m,int *y);
long kal_jul_east(int y);


/* 
 * Gregorianic calendar 
 */
long kal_conv_gre_jd(int d,int m,int y);
void kal_conv_jd_gre(long jd,int *d,int *m,int *y);
long kal_gre_east(int y);


/* 
 * Arabic calendar 
 * ("h" means the arabic year, "The Year of Hegira".)
 */
long kal_conv_ar_jd(int d,int m,int h);  
void kal_conv_jd_ar(long jd,int *d,int *m,int *h);


/* 
 * Turkish calendar 
 */
/* 
 ** Currently buggy and unusable **
long kal_conv_tur_jd (int d, int m, int h);
void kal_conv_jd_tur(long jd,int *d,int *m,int *h);
 */



/*
 * Jewish calendar
 */
long kal_jew_rosh_hashana(int y); /* Get's the jd of the first day of given
				     year (so-called "Rosh Hashana"). Used 
				     by other routines. */
long kal_conv_jew_jd(int d, int m, int y);
void kal_conv_jd_jew(long jd,int *d,int *m, int *y);
int kal_jew_leap(int y);   /* is the year leap? */


/*
 * Get the day of week of given jd, etc. [0=sun,1=mon,2=tue..] 
 */
int kal_day_of_week(long jd);
long kal_next_dw (long jd, int dw);
long kal_next_after_dw (long jd, int dw);
long kal_prev_dw (long jd, int dw);
long kal_prev_before_dw (long jd, int dw);

/*
 * Year adjustments.
 */
int kal_year_code(int y,int a);
int kal_year_decode(int y, int *a);

#endif

