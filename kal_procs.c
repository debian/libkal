/* The 'libkal' library for date conversion.
 * Copyright (C) 1996-1998 Petr Tomasek <tomasek@etf.cuni.cz>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "kal_procs.h"
#include "kal_main.h"


/*
 *  Checks whether proc valid. Returns 0 on succes.
 */

int kal_proc_valid(int proc, int sys)
{
return (!kal_procs[sys][proc]);
}

/*
 *  Get name of the calendar.
 */

char * kal_proc_get_name(int sys) 
{
return (char *)kal_procs[sys][KAL_PROCS_NAME];
}


/*
 * Basic conversion routines.
 */

long kal_conv_xxx_jd(int d, int m, int y, int sys) {
long (* fce)();
fce=kal_procs[sys][KAL_PROCS_XXX_JD];
return (* fce)(d,m,y);
} 

void kal_conv_jd_xxx(long jd, int *d, int *m, int *y, int sys) {
void (* fce)();
fce=kal_procs[sys][KAL_PROCS_JD_XXX];
(* fce)(jd,d,m,y);
}


long kal_xxx_east(int y, int sys) {
long (* fce)();
fce=kal_procs[sys][KAL_PROCS_EAST];
return (* fce)(y);
}


char * kal_xxx_months(int m, int sys) {
char * (* fce)();
fce=kal_procs[sys][KAL_PROCS_MONTHS];
return (* fce)(m);
}


