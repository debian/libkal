/* The 'libkal' library for date conversion.
 * Copyright (C) 1996-1998 Petr Tomasek <tomasek@etf.cuni.cz>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * NOTE : The transscription may vary !!
 *
 */


char * kal_jew_months(int m) {
const char *mm[]= {"Tishri","Marcheshwan","Kislew","Tebet","Shebat","Adar",
 "Adar sheni","Nisan","Ijjar","Sivan","Tamuz","Ab","Elul" };

return (char *)mm[m-1];
 }


char * kal_ar_months(int m) {
const char *mm[]= {"Muharram","Safar","Rabi' al-awwal","Rabi' ath-thani",
 "Jumada-l-ula","Jumada-l-ahira","Rajab","Sha'ban","Ramadan","Shauwal",
 "Dhu-l-qada", "Dhu-l-hijja" };

return (char *)mm[m-1];
 }

